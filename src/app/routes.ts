import { Routes } from '@angular/router';

import { PnrStatusComponent } from './pnr-status/pnr-status.component';
import { LiveStatusComponent } from './live-status/live-status.component';
import { TrainRouteComponent } from './train-route/train-route.component';
import { TrainBetweenStationsComponent } from './train-between-stations/train-between-stations.component';
import { LiveStationComponent } from './live-station/live-station.component';

export const routes: Routes = [
  {path: 'pnr-status', component: PnrStatusComponent },
  {path: 'live-status', component: LiveStatusComponent },
  {path: 'train-route', component: TrainRouteComponent},
  {path: 'train-between-stations', component: TrainBetweenStationsComponent},
  {path: 'live-station', component: LiveStationComponent},
];
