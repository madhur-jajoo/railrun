import { Component, OnInit } from '@angular/core';
import { TrainService } from '../train.service';

@Component({
  selector: 'app-pnr-status',
  templateUrl: './pnr-status.component.html',
  styleUrls: ['./pnr-status.component.css']
})
export class PnrStatusComponent implements OnInit {
  data:any;

  constructor(private trainService: TrainService) { }

  ngOnInit() {
  }

  getPnrStatus(value: string){
    this.data = false;
    this.trainService.getPnrStatus(value).subscribe(
      (result) => this.data = result,
      (err) => console.log(err)
    );
  }


}
