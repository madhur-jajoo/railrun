import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class TrainService {

  apiURL: string = 'http://api.railwayapi.com/v2/';
  apiKey: string = 'l2i4g97c16';

  constructor(private http: Http) { }

  getPnrStatus(pnr: string){
    return this.http.get(this.apiURL + 'pnr-status/pnr/' + pnr + '/apikey/' + this.apiKey + '/')
      .map((response: Response) => response.json());
  }

  getTrainStatus(train: string, date: string){
    return this.http.get(this.apiURL + 'live/train/' + train + '/date/' + date + '/apikey/' + this.apiKey + '/')
      .map((response: Response) => response.json());
  }

  getTrainRoute(train: string){
    return this.http.get(this.apiURL + 'route/train/' + train +'/apikey/' + this.apiKey + '/')
      .map((response: Response) => response.json());
  }

  getTrainBetweenStations(src: string, dest: string, date: string){
    return this.http.get(this.apiURL + 'between/source/' + src + '/dest/' + dest + '/date/' + date + '/apikey/' + this.apiKey + '/')
      .map((response: Response) => response.json());
  }

  getLiveStation(station: string, hours: string){
    return this.http.get(this.apiURL + 'arrivals/station/' + station + '/hours/' + hours + '/apikey/' + this.apiKey + '/')
      .map((response: Response) => response.json());
  }

}
