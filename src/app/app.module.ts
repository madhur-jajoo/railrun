import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { TrainService } from './train.service';

import { AppComponent } from './app.component';
import { routes } from './routes';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PnrStatusComponent } from './pnr-status/pnr-status.component';
import { LiveStatusComponent } from './live-status/live-status.component';
import { TrainRouteComponent } from './train-route/train-route.component';
import { TrainBetweenStationsComponent } from './train-between-stations/train-between-stations.component';
import { LiveStationComponent } from './live-station/live-station.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PnrStatusComponent,
    LiveStatusComponent,
    TrainRouteComponent,
    TrainBetweenStationsComponent,
    LiveStationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpModule
  ],
  providers: [TrainService],
  bootstrap: [AppComponent]
})
export class AppModule { }
