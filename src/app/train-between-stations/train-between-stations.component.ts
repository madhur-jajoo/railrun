import { Component, OnInit } from '@angular/core';

import { TrainService } from '../train.service';

@Component({
  selector: 'app-train-between-stations',
  templateUrl: './train-between-stations.component.html',
  styleUrls: ['./train-between-stations.component.css']
})
export class TrainBetweenStationsComponent implements OnInit {
  data: any;

  constructor(private trainService: TrainService) { }

  ngOnInit() {
  }

  getTrainBetweenStations(src: string, dest: string, date: string){
    this.data = false;
    let dummy = date.split('-');
    date = ''+dummy[2]+'-'+dummy[1]+'-'+dummy[0];
    this.trainService.getTrainBetweenStations(src,dest,date).subscribe(
      (result) => this.data = result,
      (err) => console.log(err)
    );
  }

}
