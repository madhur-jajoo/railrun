import { Component, OnInit } from '@angular/core';

import { TrainService } from '../train.service';

@Component({
  selector: 'app-live-status',
  templateUrl: './live-status.component.html',
  styleUrls: ['./live-status.component.css']
})
export class LiveStatusComponent implements OnInit {

  data: any;

  constructor(private trainService: TrainService) { }

  ngOnInit() {
  }

  getTrainStatus(value: string, date: string){
    this.data = false;
    let dummy = date.split('-');
    date = ''+dummy[2]+'-'+dummy[1]+'-'+dummy[0];
    this.trainService.getTrainStatus(value, date).subscribe(
      (result) => this.data = result,
      (err) => console.log(err)
    );
  }

}
