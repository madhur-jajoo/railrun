import { Component, OnInit } from '@angular/core';

import { TrainService } from '../train.service';

@Component({
  selector: 'app-train-route',
  templateUrl: './train-route.component.html',
  styleUrls: ['./train-route.component.css']
})
export class TrainRouteComponent implements OnInit {

  data: any;

  constructor(private trainService: TrainService) { }

  ngOnInit() {
  }

  getTrainRoute(value: string){
    this.data = false;
    this.trainService.getTrainRoute(value).subscribe(
      (result) => this.data = result,
      (err) => console.log(err)
    );
  }

}
