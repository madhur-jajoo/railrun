import { Component, OnInit } from '@angular/core';

import { TrainService } from '../train.service';

@Component({
  selector: 'app-live-station',
  templateUrl: './live-station.component.html',
  styleUrls: ['./live-station.component.css']
})
export class LiveStationComponent implements OnInit {
  data: any;

  constructor(private trainService: TrainService) { }

  ngOnInit() {
  }

  getLiveStation(station: string, hours: string){
    this.data = false;
    this.trainService.getLiveStation(station,hours).subscribe(
      (result) => this.data = result,
      (err) => console.log(err)
    );
  }

}
